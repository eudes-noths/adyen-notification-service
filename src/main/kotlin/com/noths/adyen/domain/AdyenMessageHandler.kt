package com.noths.adyen.domain

import arrow.core.Either
import arrow.core.flatMap
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

data class AdyenCallback(
    val pspReference: String,
    val merchantAccountCode: String,
    )

fun processCallback(
    save: (AdyenCallback) -> Either<String, AdyenCallback>,
    postMessage: (AdyenCallback) -> Either<String, AdyenCallback>
): (AdyenCallback) -> Either<String, AdyenCallback> {
    return { callback: AdyenCallback ->
        log.info("AdyenMessageHandler processCallback()")
        save(callback).flatMap(postMessage)
    }
}
