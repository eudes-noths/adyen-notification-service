package com.notonthehighstreet.services.kotlin.adyen.spring

import arrow.core.Either
import com.noths.adyen.domain.AdyenCallback
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

fun save(callback: AdyenCallback): Either<String, AdyenCallback> {
    log.info("Database save()")
    return Either.Left("save error")
}

fun saveString(otherStuff: String): String {
    return "Not the function you're looking for"
}
