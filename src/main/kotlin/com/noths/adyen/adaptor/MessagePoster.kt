package com.notonthehighstreet.services.kotlin.adyen.spring

import arrow.core.Either
import com.noths.adyen.domain.AdyenCallback
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

fun postMessage(adyenCallback: AdyenCallback): Either<String, AdyenCallback>  {
    log.info("MessagePoster postMessage()")
    return Either.Left("postMessage error")
}
