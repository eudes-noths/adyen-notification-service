package com.noths.plugins

import io.ktor.routing.*
import io.ktor.http.*
import com.noths.adyen.domain.AdyenCallback
import com.noths.adyen.domain.processCallback
import com.notonthehighstreet.services.kotlin.adyen.spring.postMessage
import com.notonthehighstreet.services.kotlin.adyen.spring.save
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.configureRouting() {
    val processCb = processCallback(::save, ::postMessage)

    // Starting point for a Ktor app:
    routing {
        get("/") {
            val request = AdyenCallback("pspReference", "merchantAccountCode")
            val result = processCb(request)
            log.info("processCallback returned: $result")
            call.respondText("Hello World!")
        }
    }
}
